# JavaScript: Getting Started
by Mark Zamoyta

JavaScript is the popular programming language which powers web pages and web applications. If you are new to programming or just new to the language, this course will get you started coding in JavaScript.